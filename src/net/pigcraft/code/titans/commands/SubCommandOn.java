package net.pigcraft.code.titans.commands;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public final class SubCommandOn implements ITitanSubCommand
{
    @Override
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (sender instanceof Player)
        {
            if (args.length == 1)
            {
                Titan titan = Titans.plugin.getTitan(args[0]);

                if (titan == null)
                {
                    sender.sendMessage(ChatColor.GRAY + "Error! " + ChatColor.RED + args[0] + ChatColor.GRAY + " is not a Titan!");
                    return false;
                }

                Titans.plugin.setPlayerAsTitan((Player)sender, titan);
                return true;
            }
            else
            {
                sender.sendMessage(CommandTitan.SYNTAX_ERROR);
                return false;
            }
        }
        else
        {
            sender.sendMessage(ChatColor.RED + "We're sorry, but you must be a player to do that!");
            return false;
        }
    }

    @Override
    public String getName()
    {
        return "on";
    }
}
