package net.pigcraft.code.titans.commands;

import org.bukkit.command.CommandSender;

public interface ITitanSubCommand
{

    boolean onCommand(final CommandSender sender, final String[] args);
    String getName();

}
