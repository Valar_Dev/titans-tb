package net.pigcraft.code.titans.skills;

import net.pigcraft.code.titans.Titan;
import org.bukkit.event.Listener;

public abstract class TitanSkill implements Listener
{


    private final String name;
    private final Titan titan;
    private Cooldown cooldown;

    private String description;

    public TitanSkill(final String name, final Titan titan)
    {
        this.name = name;
        this.titan = titan;

        this.description = "";
    }

    public TitanSkill(final String name, final Titan titan, final Cooldown cooldown)
    {
        this.name = name;
        this.titan = titan;
        this.cooldown = cooldown;

        this.description = "";
    }

    public final String getName()
    {
        return name;
    }

    public final Titan getTitan()
    {
        return titan;
    }

    public final String getDescription()
    {
        return description;
    }

    public final Cooldown getCooldown()
    {
        return cooldown;
    }

    public final void setDescription(final String description)
    {
        this.description = description;
    }


    public abstract void use();

    @Override
    public final String toString()
    {
        return this.getTitan().getName() + "::" + this.getName();
    }

}
