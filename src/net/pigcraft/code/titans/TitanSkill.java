package net.pigcraft.code.titans;

public abstract class TitanSkill
{


    private final String name;

    public TitanSkill(final String name)
    {
        this.name = name;
    }

    public final String getName()
    {
        return name;
    }

    public abstract void use();

}
