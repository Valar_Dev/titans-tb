package net.pigcraft.code.titans.builtins.avistlo;

//import org.bukkit.Bukkit;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
//import org.bukkit.Location;



import org.bukkit.Location;
import org.bukkit.entity.Player;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.TitanSkill;

public final class ForceOfLight extends TitanSkill
{
    public ForceOfLight(final Titan titan)
    {
        super("light", titan, new Cooldown(20));

        this.setDescription(ChatColor.GRAY + "Name: " + ChatColor.AQUA + "Force of Light"
        + "\n           Avistlo focuses light into a deadly attack!");
    }

	@SuppressWarnings("deprecation")
	@Override
    public void use()
    {
        ArrayList<Player> nearbyPlayers = playersDistance(this.getTitan().getController().getTargetBlock(null, 200).getLocation(), 7);
        SphereLight(this.getTitan().getController().getTargetBlock(null, 200).getLocation(), 4, 4, false, true, 0);
        for(Player player : nearbyPlayers)
        {
        	if(player!=this.getTitan().getController())
        	{
        		player.setFireTicks(20);
        		Location loc = player.getLocation();
            	Location spawn1 = new Location(loc.getWorld(), loc.getX() - 1,loc.getY(), loc.getZ());
            	Location spawn2 = new Location(loc.getWorld(), loc.getX(),loc.getY(), loc.getZ() + 1);
            	Location spawn3 = new Location(loc.getWorld(), loc.getX(),loc.getY(), loc.getZ() - 1);
            	Location spawn4 = new Location(loc.getWorld(), loc.getX() + 1,loc.getY(), loc.getZ());
            	player.getWorld().strikeLightning(spawn1);
            	player.getWorld().strikeLightning(spawn2);
            	player.getWorld().strikeLightning(spawn3);
            	player.getWorld().strikeLightning(spawn4);
        	}
        }
		this.getTitan().getController().getWorld().strikeLightning(this.getTitan().getController().getTargetBlock(null, 200).getLocation());
		this.getTitan().getController().getWorld().strikeLightning(this.getTitan().getController().getLocation());
		SphereLight(this.getTitan().getController().getLocation(), 2, 2, false, true, 0);
    }
   
	 public static ArrayList<Player> playersDistance(Location loc,
	    		double radius)
	    	{
	    		 
	    		ArrayList<Player> players = new ArrayList<Player>();
	    		 
	    		double i1 = loc.getX();
	    		double j1 = loc.getY();
	    		double k1 = loc.getZ();
	    		 
	    		for (Player player : Bukkit.getOnlinePlayers()) {
	    		 
	    		if (player.getWorld().equals(loc.getWorld())) {
	    		 
	    		double i2 = player.getLocation().getX();
	    		double j2 = player.getLocation().getY();
	    		double k2 = player.getLocation().getZ();
	    		 
	    		double ad = Math.sqrt((i2 - i1)*(i2 - i1) + (j2 - j1)*(j2 - j1) + (k2 - k1)*(k2 - k1));
	    		 
	    		if (ad < radius) {
	    		players.add(player);
	    		}
	    		 
	    		}
	    		 
	    		}
	    		 
	    		return players;
	    		 
	    	}
	 public static void SphereLight (Location loc, Integer r, Integer h, Boolean hollow, Boolean sphere, int plus_y) {
         int cx = loc.getBlockX();
         int cy = loc.getBlockY();
         int cz = loc.getBlockZ();
         for (int x = cx - r; x <= cx +r; x++)
             for (int z = cz - r; z <= cz +r; z++)
                 for (int y = (sphere ? cy - r : cy); y < (sphere ? cy + r : cy + h); y++) {
                     double dist = (cx - x) * (cx - x) + (cz - z) * (cz - z) + (sphere ? (cy - y) * (cy - y) : 0);
                     if (dist < r*r && !(hollow && dist < (r-1)*(r-1))) {
                         Location l = new Location(loc.getWorld(), x, y + plus_y, z);
                         	loc.getWorld().strikeLightning(l);
                         }
                     }
  
     }
}
