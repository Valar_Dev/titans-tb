package net.pigcraft.code.titans.builtins.smdp;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;
import org.bukkit.ChatColor;
import org.bukkit.Location;

import java.util.Random;

public final class SuperMechaDeathPika extends Titan
{

    private static final Random random = new Random();

    public SuperMechaDeathPika()
    {
        super(Titans.plugin, "smdp", "Pika...bzzt...chu!", "Chuuuuuuuuu... bzzt!", ChatColor.YELLOW + "Super Mecha Death pika : " + ChatColor.GOLD + "$m");

        //TODO Should probably load this from a text file. Lazy :P
        this.setDescription("Pikachu used for experiments in the Pokemon Lab on Cinnabar Island. " +
                "When a volcanic eruption on the island destroyed everything except the Pokémon Center, " +
                "the Pikachu fused with laboratory equipment to become a mechanical beast. Friendly until" +
                " attacked, at which point it will retaliate without hesitation.");
    }

    @Override
    public void onEntrance()
    {


        final Location loc = this.getController().getLocation();

        // I looked up the definition of the world 'several' - 7 :p
        for (int i = 0; i < 7; i++)
        {
            this.getController().getWorld().strikeLightningEffect(new Location(getController().getWorld(), (loc.getX() + (double)random.nextInt(4)), loc.getY(), (loc.getZ() + (double)random.nextInt(4))));
        }

    }

    @Override
    public void onExit()
    {
        final Location loc = this.getController().getLocation();

        // Didn't look flashy enough with only 7
        for (int i = 0; i < 10; i++)
        {
            this.getController().getWorld().createExplosion((loc.getX() + (double)random.nextInt(4)), loc.getY(), (loc.getZ() + (double)random.nextInt(4)), 0.0F, false);
        }
    }

    @Override
    public void onLoad()
    {
        this.addSkill(new LightningBallSkill(this));
    }

    @Override
    public void onUnload()
    {
        this.clearSkills();
    }
}
